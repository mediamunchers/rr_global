<?php
?><!DOCTYPE html>
<!--[if IE 7]><html class="ie ie7" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 8]><html class="ie ie8" <?php language_attributes(); ?>><![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title><?php wp_title( '|', true, 'right' ); ?><?php bloginfo('name'); ?></title>
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php 
global $woocommerce;
if ( sizeof( $woocommerce->cart->cart_contents) > 0 ) { 
?>
<div id="checkout">
	<a href="<?php echo $woocommerce->cart->get_cart_url() ?>" class="btn btn-filled" title="<?php _e( 'Checkout' ) ?>"><?php _e( 'Your Booking', 'redrock' ) ?></a>
</div>
<?php } ?>

<div class="header">
	
	<div class="ms-nav">
		<ul>
			<li><a href="http://www.janvanghent.com"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/logo-jvg.png" class="no-resp" alt="" /></a></li>
			<li class="active"><a href="<?php bloginfo('home'); ?>"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/logo-dpp.png" class="no-resp" alt="" /></a></li>
			<li><a href="http://www.waterlelies.com"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/logo-dwl.png" class="no-resp" alt="" /></a></li>
		</ul>
	</div>

	<div class="site-nav">
		<div class="nav-dsktp">
			<?php
			$site_nav_args = array(
				'theme_location'  => 'site-nav',
				'menu'            => '',
				'container'       => '',
				'container_class' => '',
				'container_id'    => '',
				'menu_class'      => 'sitenav',
				'menu_id'         => '',
				'echo'            => true,
				'before'          => '',
				'after'           => '',
				'link_before'     => '',
				'link_after'      => '',
				'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
				'depth'           => 0,
				'walker'          => ''
			);

			wp_nav_menu( $site_nav_args );
			?>
		</div>
		<div class="nav-mbl">
			- Menu -

			<div class="mbl-site-nav">
				<?php
				$site_nav_args = array(
					'theme_location'  => 'site-nav',
					'menu'            => '',
					'container'       => '',
					'container_class' => '',
					'container_id'    => '',
					'menu_class'      => 'sitenav',
					'menu_id'         => '',
					'echo'            => true,
					'before'          => '',
					'after'           => '',
					'link_before'     => '',
					'link_after'      => '',
					'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
					'depth'           => 0,
					'walker'          => ''
				);

				wp_nav_menu( $site_nav_args );
				?>
			</div>

		</div>
	</div>

</div>