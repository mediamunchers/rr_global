<li>
	<h2 class="page-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
	<span class="date"><?php the_author(); ?> - <?php the_date(); ?></span>
	<?php if ( has_post_thumbnail() ) { ?>
	<div class="image">
		<a href="<?php the_permalink(); ?>">
			<?php the_post_thumbnail( 'large' ); ?>
		</a>
	</div>
	<?php } ?>
	<?php the_excerpt(); ?>

	<a href="<?php the_permalink(); ?>" class="btn-filled"><?php _e('Read More', 'redrock'); ?></a>
</li>