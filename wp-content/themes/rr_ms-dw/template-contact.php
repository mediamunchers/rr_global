6<?php
/*
Template name: Contact
*/
?>

<?php get_header(); ?>

<div class="row-padding txt-cntr introduction">
	<div class="wrap">

		<?php if( have_posts() ) :
			while( have_posts() ) :
				the_post() ;
		?>

		<h1><?php _e('Contact us', 'redrock'); ?></h1>
		<?php the_content(); ?>

		<?php endwhile;
		endif; ?>
	</div>
</div>
<div class="row-padding txt-cntr">
	<div class="wrap">
		<?php echo do_shortcode( '[contact-form-7 id="214" title="Contact NL"]' ) ?>
	</div>
</div>


<div class="map row-padding txt-cntr">
	<div style="padding: 40px 40px 0 40px;">
		<div id="map-canvas"></div>
	</div>
</div>

<div class="contact-bar">

	<div class="row-full">
		<div class="one-third txt-cntr">
			<h3><?php _e('Address', 'redrock'); ?></h3>
			<p>Vliegpleinkouter 35/201<br />
			9030 Mariakerke<br />
			<?php _e('Belgium', 'redrock'); ?></p>
		</div>
		<div class="one-third txt-cntr">
			<h3><?php _e('Contact Details', 'redrock'); ?></h3>
			<p>Tel.: +32 (0) 475 75 30 59<br />
			<a href="mailto:info@janvanghent.com">info@janvanghent.com</a></p>
		</div>
		<div class="one-third soc-med txt-cntr">
			<h3><?php _e('Social Media', 'redrock'); ?></h3>
			<a href="https://www.facebook.com/janvanghent" target="_blank"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/fb-large.png" alt="Facebook" /></a>
			<a href="https://foursquare.com/v/jan-van-ghent/53a31268498e3f7fb3122089" target="_blank"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/foursquare-large.png" alt="Foursquare" /></a>
		</div>
	</div>

</div>

<div class="newsletter-optin">

	<div class="wrap row-padding txt-cntr">
		<h2><?php _e('Our newsletter', 'redrock'); ?></h2>
		<p><?php _e('Subscribe to our newsletter and stay informed about our upcoming activiteits and promotions.', 'redrock'); ?></p>		
	
		<div class="row">
			<?php echo do_shortcode('[mc4wp_form]'); ?>
		</div>

	</div>
</div>

<?php get_footer(); ?>