<?php
/*
Template name: Concept
*/
?>

<?php get_header(); ?>
<div class="row-padding txt-cntr introduction">
	<div class="wrap">

		<?php if( have_posts() ) :
			while( have_posts() ) :
				the_post() ;
		?>

		<h1><?php the_title(); ?></h1>
		<?php the_content(); ?>

		<?php endwhile;
		endif; ?>
	</div>
</div>
<div class="row-padding txt-cntr">
	<div class="wrap">

		<?php 

		$args = array(
			'post_type' => 'concept',
			'order' => 'asc'
		);
		
		$concepts = new WP_Query( $args );
		if ( $concepts->have_posts() ) :
			while ( $concepts->have_posts() ) : $concepts->the_post();
			?>
			<?php get_template_part('content', 'concept' ); ?>
			<?php
			endwhile;
		endif;
		wp_reset_postdata();
		?>

	</div>
</div>


<div class="newsletter-optin">

	<div class="wrap row-padding txt-cntr">
		<h2><?php _e('Our newsletter', 'redrock'); ?></h2>
		<p><?php _e('Subscribe to our newsletter and stay informed about our upcoming activiteits and promotions.', 'redrock'); ?></p>		
		
		<div class="row">
			<?php echo do_shortcode('[mc4wp_form]'); ?>
		</div>

	</div>
</div>

<?php get_footer(); ?>