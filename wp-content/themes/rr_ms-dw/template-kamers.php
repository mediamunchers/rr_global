<?php
/*
Template name: Kamers
*/
?>

<?php get_header(); ?>

<div class="row-padding txt-cntr">
	<div class="wrap">

		<?php if( have_posts() ) :
			while( have_posts() ) :
				the_post() ;
		?>

		<h1><?php the_title(); ?></h1>

		<?php endwhile;
		endif;

		$args = array(
	        'posts_per_page' => -1,
	        'post_type' => 'product',
            'orderby' => 'meta_value', 
            'meta_key' => '_price',
	        'order' => 'ASC',
	    );
					    	
		$rooms = new WP_Query( $args );
		if ( $rooms->have_posts() ) :
			while ( $rooms->have_posts() ) : $rooms->the_post();
			?>
				<div class="concept">
					<div class="image"><? the_post_thumbnail( 'medium' ); ?></div>
					<div class="text">
						<h3><?php the_title(); ?></h3>
						<?php the_excerpt(); ?>
						<a href="<?php the_permalink(); ?>" class="btn-filled"><?php _e('Book Now', 'redrock'); ?></a>
					</div>
				</div>
			<?php
			endwhile;
		endif;
		wp_reset_postdata();
		?>

	</div>
</div>


<div class="newsletter-optin">

	<div class="wrap row-padding txt-cntr">
		<h2><?php _e('Our newsletter', 'redrock'); ?></h2>
		<p><?php _e('Subscribe to our newsletter and stay informed about our upcoming activiteits and promotions.', 'redrock'); ?></p>		
		<div class="row">
			<?php echo do_shortcode('[mc4wp_form]'); ?>
		</div>

	</div>
</div>

<?php get_footer(); ?>