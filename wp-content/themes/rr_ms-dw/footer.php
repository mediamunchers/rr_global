<div class="footer">
	<div class="wrap">

		<div class="left">
			<?php _e('All rights reserved. We hope to see you soon!', 'redrock'); ?>
		</div>

		<div class="right">
			<a href="<?php echo get_permalink(232); ?>"><?php _e('House Rules', 'redrock'); ?></a>    -    <a href="<?php echo get_permalink(26); ?>"><?php _e('Contact', 'redrock'); ?></a>
		</div>

	</div>
</div>
<div class="footer-bis">
	<a href="#"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/logo-rr.png" alt="Red Rock Website"/></a>
</div>
<?php wp_footer(); ?>

</body>
</html>