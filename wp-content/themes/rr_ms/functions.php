<?php

/*-----------------------------------------------------------------------------------*/
/* Remove Header Links */
/*-----------------------------------------------------------------------------------*/
remove_action( 'wp_head', 'wlwmanifest_link' );
remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
remove_action( 'wp_head', 'rel_canonical' ); // Display the canonical link rel
remove_action( 'wp_head', 'wp_generator' ); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );


/*-----------------------------------------------------------------------------------*/
/* Theme Settings */
/*-----------------------------------------------------------------------------------*/
// Disable admin-bar
add_filter('show_admin_bar', '__return_false');

// Post Thumbnail Init
add_theme_support( 'post-thumbnails' );

// WooCommerce Prices
add_filter( 'woocommerce_price_trim_zeros', function() { return false; } );

/*-----------------------------------------------------------------------------------*/
/* Project Enqueues */
/*-----------------------------------------------------------------------------------*/

// JAVASCRIPT
add_action( 'wp_enqueue_scripts', 'custom_enqueue_scripts' ); 
function custom_enqueue_scripts() {

  // jQuery
  wp_deregister_script('jquery');
  wp_register_script( 'jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js');
  wp_enqueue_script( 'jquery-ui-js', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js' );
  wp_enqueue_script( 'jquery' );

  // Custom JS
  wp_enqueue_script( 'custom-js', get_template_directory_uri() . '/assets/js/custom.min.js' );

  // Fancybox
  wp_enqueue_script( 'fancybox-js', get_template_directory_uri() . '/assets/js/jquery.fancybox.js', array(), '', true );
  wp_enqueue_script( 'fancybox-helper-buttons-js', get_template_directory_uri() . '/assets/js/helpers/jquery.fancybox-buttons.js', array(), '', true );
  wp_enqueue_script( 'fancybox-media-buttons-js', get_template_directory_uri() . '/assets/js/helpers/jquery.fancybox-media.js', array(), '', true );
  wp_enqueue_script( 'fancybox-thumbs-buttons-js', get_template_directory_uri() . '/assets/js/helpers/jquery.fancybox-thumbs.js', array(), '', true );

}

// CSS
add_action( 'wp_enqueue_scripts', 'custom_enqueue_styles' ); 
function custom_enqueue_styles() {

  // Google Fonts
  wp_register_style('google-bitter', 'http://fonts.googleapis.com/css?family=Bitter:400,700');
  wp_enqueue_style( 'google-bitter');

  wp_register_style('google-satisfy', 'http://fonts.googleapis.com/css?family=Satisfy');
  wp_enqueue_style( 'google-satisfy');

  // Custom CSS
  wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css', '', '1.1', all );


  // Fancybox CSS
  wp_enqueue_style( 'fancybox', get_template_directory_uri() . '/assets/css/jquery.fancybox.css' );
  wp_enqueue_style( 'fancybox-buttons', get_template_directory_uri() . '/assets/js/helpers/jquery.fancybox-buttons.css' );
  wp_enqueue_style( 'fancybox-thumbs', get_template_directory_uri() . '/assets/js/helpers/jquery.fancybox-thumbs.css' );

}


/*-----------------------------------------------------------------------------------*/
/* Selectively Hide WP Admin Menus */
/*-----------------------------------------------------------------------------------*/
// add_action('admin_init', 'ns_remove_admin_menus');
// function ns_remove_admin_menus() {

//   global $current_user;

//   if ($current_user->ID != 2) {

//     // Remove menu pages
//     //remove_menu_page('edit.php');
//     remove_menu_page('plugins.php');
//     remove_menu_page('options-general.php');
//     remove_menu_page('tools.php');
//     remove_menu_page('edit.php?post_type=acf');
//     remove_submenu_page( 'themes.php', 'themes.php' );
//     remove_submenu_page( 'themes.php', 'customize.php' );
//     remove_submenu_page( 'themes.php', 'theme-editor.php' );

//     // Remove WP Update nag
//     add_action('admin_menu','wphidenag');
//     function wphidenag() {
//       remove_action( 'admin_notices', 'update_nag', 3 );
//     }

//     // Remove Plugin Update Nag
//     remove_action( 'load-update-core.php', 'wp_update_plugins' );
//     add_filter( 'pre_site_transient_update_plugins', create_function( '$a', "return null;" ) );

//   } // endif;

// }


/*-----------------------------------------------------------------------------------*/
/* Menus init */
/*-----------------------------------------------------------------------------------*/
add_action( 'init', 'register_menus' );

function register_menus() {
  register_nav_menus(
    array(
      'site-nav' => __( 'Site Navigation', 'custom_theme' )
    )
  );
}



/*-----------------------------------------------------------------------------------*/
/* Image Sizes */
/*-----------------------------------------------------------------------------------*/
//add_image_size( 'header', 1600, true );
//update_option('thumbnail_size_w', 280);
//update_option('thumbnail_size_h', 168);
//update_option('medium_size_w', 500);
//update_option('medium_size_h', 300);
//update_option('large_size_w', 680);
//update_option('large_size_h', 410);


/*-----------------------------------------------------------------------------------*/
/* Google Maps JS */
/*-----------------------------------------------------------------------------------*/
function my_custom_js() {
  if( is_page( array(52, 131, 161, 171, 156, 164) ) ){ /* !!!!!!!!!! INSERT CONTACT PAGE ID HERE !!!!!!!!!! */
    echo'
    <script type="text/javascript"
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC3WvbGhGu6pkYWVwBYIAt7J2hn-Hbowjo&sensor=false">
    </script>
    <script type="text/javascript">
      function gmaps_init() {

      var latlng = new google.maps.LatLng(51.069879, 3.667510);

      var styles = [ { featureType: "water", elementType: "all", stylers: [ { hue: "#749fae" }, { saturation: -41 }, { lightness: -25 }, { visibility: "on" } ] },{ featureType: "landscape", elementType: "geometry", stylers: [ { hue: "#f6f0e4" }, { saturation: 32 }, { lightness: 36 }, { visibility: "on" } ] },{ featureType: "water", elementType: "all", stylers: [ ] },{ featureType: "road.highway", elementType: "all", stylers: [ { hue: "#c4c1bb" }, { saturation: -93 }, { lightness: 31 }, { visibility: "on" } ] } ];

      var myOptions = {

        scrollwheel: false,
        navigationControl: true,
        mapTypeControl: true,
        scaleControl: true,
        draggable: true,

        zoom: 12,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        disableDefaultUI: false,
        styles: styles
      };

      var map = new google.maps.Map(document.getElementById("map-canvas"), myOptions);
      
      var image = "' . get_bloginfo('stylesheet_directory') . '/images/map-marker.png";
      var marker = new google.maps.Marker({
          position: latlng,
          map: map,
          icon: image
      });
    }

    google.maps.event.addDomListener(window, "load", gmaps_init);
    </script>

    ';
  }
}
add_action('wp_head', 'my_custom_js');


/* ------------------------------------------------------------------*/
/* ADD REL ATTRIBUTE FOR LIGHTBOX */
/* ------------------------------------------------------------------*/
 
add_filter('wp_get_attachment_link', 'rc_add_rel_attribute');
function rc_add_rel_attribute($link) {
  global $post;
  return str_replace('<a href', '<a rel="gallery" href', $link);
}  
add_filter('wp_get_attachment_link', 'add_image_overlay');
function add_image_overlay($link) {
  global $post;
  return str_replace('</a>', '<span class="main-gallery-overlay"></span></a>', $link);
} 

/*-----------------------------------------------------------------------------------*/
/* CPT CONCEPT */
/*-----------------------------------------------------------------------------------*/
add_action( 'init', 'custom_post_type_concept' );

function custom_post_type_concept() {
      
  $labels = array(
    'name' => __('Concept', 'redrock'),
    'singular_name' => __('Concept', 'redrock'),
    'add_new' => __('Toevoegen', 'redrock'),
    'add_new_item' => __('Concept', 'redrock'),
    'edit_item' => __('Bewerken', 'redrock'),
    'new_item' => __('Nieuw concept', 'redrock'),
    'view_item' => __('Bekijk concept', 'redrock'),
    'search_items' => __('Zoek naar concepten', 'redrock'),
    'not_found' =>  __('Geen concepten gevonden', 'redrock'),
    'not_found_in_trash' => __('Geen concepten gevonden in Trash', 'redrock'),
    'parent_item_colon' => ''
  );

  $supports = array(
    'title',
    'editor',
    'categories',
    'excerpt',
    'thumbnail'
  );
  
  register_post_type( 'concept',
    array(
      'labels' => $labels,
      'public' => true,
      'hierarchical' => false,
      'has_archive' => true,
      'menu_icon' => 'dashicons-megaphone',
      'supports' => $supports
    )
  );
  
}

/*-----------------------------------------------------------------------------------*/
/* Custom Admin CSS */
/*-----------------------------------------------------------------------------------*/
function custom_admin_css() {
   echo '<style type="text/css">
           .shipping_tab{display:none !important;}
           .attribute_tab{display:none !important;}
           .linked_product_tab{display:none !important;}
           .advanced_tab{display:none !important;}
         </style>';
}

add_action('admin_head', 'custom_admin_css');

/*-----------------------------------------------------------------------------------*/
/* Includes */
/*-----------------------------------------------------------------------------------*/
//require_once ( 'includes/cpts.php' );
//require_once ( 'includes/sidebars.php' );
//require_once ( 'includes/widgets.php' );
require_once ( 'includes/shortcodes.php' );

?>
