<?php
/*
Template name: Arrangementen
*/
$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
if (strpos($url,'/nl/') !== false) {
    $cat = "kamers";
} else {
    $cat = "kamers-en";
}
?>

<?php get_header(); ?>
<div class="row-padding txt-cntr introduction">
	<div class="wrap">

		<?php if( have_posts() ) :
			while( have_posts() ) :
				the_post() ;
		?>

		<h1><?php the_title(); ?></h1>
		<?php the_content(); ?>

		<?php endwhile;
		endif; ?>
	</div>
</div>
<div class="row-padding txt-cntr">
	<div class="wrap">
		<div class="row availability">
			<div class="full-width clearfix">
				<div class="one-half">
					<label for="av_checkin"><?php _e('Check-in', 'redrock'); ?></label>
					<input type="text" class="datepicker" id="av_checkin" name="av_checkin">
					<input type="hidden" id="alt_av_checkin">
				</div>
				<div class="one-half">
					<label for="av_checkout"><?php _e('Check-out', 'redrock'); ?></label>
					<input type="text" class="datepicker" id="av_checkout" name="av_checkout">
					<input type="hidden" id="alt_av_checkout">
				</div>
			</div>
			<div class="full-width txt-cntr">
				<input type="button" class="btn btn-filled" id="av_check" value="<?php _e('Check availability', 'redrock'); ?>">
			</div>
		</div>

		<?php 
		$args = array(
	        'posts_per_page' => -1,
	        'post_type' => 'product',
	        'product_cat' => $cat,
            'orderby' => 'meta_value', 
            'meta_key' => '_price',
	        'order' => 'ASC',
	    );

		$rooms = new WP_Query( $args );
		if ( $rooms->have_posts() ) :
			while ( $rooms->have_posts() ) : $rooms->the_post();
			$product = get_the_title();
			/**START AVAILABILITY CHECK**/
			$product_id = $post->ID;
			$fully_booked_days = array();
			$find_bookings_for = array( $product_id );

			$existing_bookings  = WC_Bookings_Controller::get_bookings_for_objects(
				$find_bookings_for,
				array(
					'unpaid',
					'pending',
					'confirmed',
					'paid',
					'complete'
				)
			);

			foreach ( $existing_bookings as $existing_booking ) {
				$start_date  = $existing_booking->start;
				$end_date    = $existing_booking->is_all_day() ? strtotime( 'tomorrow midnight', $existing_booking->end ) : $existing_booking->end;
				$product_id  = $existing_booking->get_product_id();
				$resource_id = $existing_booking->get_resource_id();
				$check_date  = $start_date; // Take it from the top
				
				// Loop over all booked days in this booking
				while ( $check_date < $end_date ) {
					if ( $check_date >= current_time( 'timestamp' ) ) {
						$js_date = date( 'Y-n-j', $check_date );
							// Skip if we've already found this product is unavailable
							if ( ! empty( $fully_booked_days[ $js_date ] ) ) {
								$check_date = strtotime( "+1 day", $check_date );
								continue;
							}

							//if ( ! $post->has_available_block_within_range( strtotime( 'midnight', $check_date ), strtotime( 'tomorrow midnight', $check_date ), 0 ) ) {
								$fully_booked_days[ $js_date ][0] = true;
							//}						
					}
					$check_date = strtotime( "+1 day", $check_date );
				}
				
			}

			/**END AVAILABILITY CHECK**/
			?>
				<div class="reservation-item" id="<?php the_ID(); ?>" data-fully-booked-days="<?php echo esc_attr( json_encode( $fully_booked_days ) ); ?>">
					<div class="image"><? the_post_thumbnail( 'thumb' ); ?></div>
					<div class="text">
						<h3><?php echo $product; ?></h3>
						<a href="<?php the_permalink(); ?>" class="btn-filled"><?php _e('Book this room', 'redrock'); ?></a>
					</div>
				</div>
			<?php
			endwhile;
		endif;
		wp_reset_postdata();
		?>

	</div>
</div>


<div class="newsletter-optin">

	<div class="wrap row-padding txt-cntr">
		<h2><?php _e('Our newsletter', 'redrock'); ?></h2>
		<p><?php _e('Subscribe to our newsletter and stay informed about our upcoming activiteits and promotions.', 'redrock'); ?></p>		
		<div class="row">
			<?php echo do_shortcode('[mc4wp_form]'); ?>
		</div>

	</div>
</div>

<?php get_footer(); ?>