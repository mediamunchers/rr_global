<div class="concept" id="<?php echo get_field('concept_url'); ?>">
	<div class="image"><? the_post_thumbnail( 'medium' ); ?></div>
	<div class="text">
		<h3><?php the_title(); ?></h3>
		<?php the_content(); ?>
		<a href="<?php the_permalink(); ?>" class="btn-filled"><?php _e('Read More', 'redrock'); ?></a>
	</div>
</div>