<?php
/**
 * Booking product add to cart
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $woocommerce, $product;

if ( ! $product->is_purchasable() ) {
	return;
}

do_action( 'woocommerce_before_add_to_cart_form' ); ?>

<noscript><?php _e( 'Your browser must support JavaScript in order to make a booking.', 'woocommerce-bookings' ); ?></noscript>

<form class="cart" method="post" enctype='multipart/form-data'>

	<?php 
	$beds24 = get_field('beds24_kamernummer');
	echo do_shortcode('[beds24-box roomid="'.$beds24.'"  href="http://www.janvanghent.com"]'); 
	?>

</form>

<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>
