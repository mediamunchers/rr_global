<?php
/*
Template name: Blog
*/
?>

<?php get_header(); ?>

<div class="row-padding">
	<div class="wrap">

		<?php

			$blogArgs = array(
				'post_type' => 'post'
			);

			$blogs = new WP_Query( $blogArgs );

			?>

			<?php if( $blogs->have_posts() ) :
			?><ul class="blogposts txt-cntr"><?php
				while( $blogs->have_posts() ) :
					$blogs->the_post();
				?>

					<?php get_template_part('content', 'blog' ); ?>

				<?php
				endwhile;
				?></ul><?php
			endif;
			?>

	</div>
</div>


<div class="newsletter-optin">

	<div class="wrap row-padding txt-cntr">
		<h2><?php _e('Our newsletter', 'redrock'); ?></h2>
		<p><?php _e('Subscribe to our newsletter and stay informed about our upcoming activiteits and promotions.', 'redrock'); ?></p>		
		<div class="row">
			<?php echo do_shortcode('[mc4wp_form]'); ?>
		</div>

	</div>
</div>

<?php get_footer(); ?>