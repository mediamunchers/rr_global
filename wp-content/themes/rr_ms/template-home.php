<?php
/*
Template name: Homepage
*/
?>

<?php get_header("home"); ?>

<div class="introduction row-padding txt-cntr">
	<div class="wrap">

		<?php if( have_posts() ) :
			while( have_posts() ) :
				the_post() ;
		?>

		<?php the_content(); ?>

		<?php endwhile;
		endif; ?>


	</div>
</div>

<div class="assets txt-cntr">
	<h1><?php _e("Troeven", "redrock"); ?></h1>
	<div class="row row-padding">

		<div class="asset one-fourth txt-cntr">
			<div class="thumb">
				<?php
				$troef_1_img = get_field('troef_1_img');
				$troef_1_thumb = $troef_1_img['sizes']['thumbnail'];
				?>
				<img src="<?php echo $troef_1_thumb; ?>" alt="Troef <?php the_field('troef_1_titel'); ?>" />
			</div>
			<div class="content">
				<h3><?php the_field('troef_1_titel'); ?></h3>
				<p class="main"><?php the_field('troef_1_tekst'); ?></p>
				<a href="<?php echo get_permalink(173).get_field('troef_1_url'); ?>" class="btn btn-filled"><?php _e('Read More', 'redrock'); ?></a>
			</div>
		</div>

		<div class="asset one-fourth txt-cntr">
			<div class="thumb">
				<?php
				$troef_2_img = get_field('troef_2_img');
				$troef_2_thumb = $troef_2_img['sizes']['thumbnail'];
				?>
				<img src="<?php echo $troef_2_thumb; ?>" alt="Troef <?php the_field('troef_2_titel'); ?>" />
			</div>
			<div class="content">
				<h3><?php the_field('troef_2_titel'); ?></h3>
				<p class="main"><?php the_field('troef_2_tekst'); ?></p>
				<a href="<?php echo get_permalink(173).get_field('troef_2_url'); ?>" class="btn btn-filled"><?php _e('Read More', 'redrock'); ?></a>
			</div>
		</div>

		<div class="asset one-fourth txt-cntr">
			<div class="thumb">
				<?php
				$troef_3_img = get_field('troef_3_img');
				$troef_3_thumb = $troef_3_img['sizes']['thumbnail'];
				?>
				<img src="<?php echo $troef_3_thumb; ?>" alt="Troef <?php the_field('troef_3_titel'); ?>" />
			</div>
			<div class="content">
				<h3><?php the_field('troef_3_titel'); ?></h3>
				<p class="main"><?php the_field('troef_3_tekst'); ?></p>
				<a href="<?php echo get_permalink(173).get_field('troef_3_url'); ?>" class="btn btn-filled"><?php _e('Read More', 'redrock'); ?></a>
			</div>
		</div>

		<div class="asset one-fourth txt-cntr">
			<div class="thumb">
				<?php
				$troef_4_img = get_field('troef_4_img');
				$troef_4_thumb = $troef_4_img['sizes']['thumbnail'];
				?>
				<img src="<?php echo $troef_4_thumb; ?>" alt="Troef <?php the_field('troef_4_titel'); ?>" />
			</div>
			<div class="content">
				<h3><?php the_field('troef_4_titel'); ?></h3>
				<p class="main"><?php the_field('troef_4_tekst'); ?></p>
				<a href="<?php echo get_permalink(173).get_field('troef_4_url'); ?>" class="btn btn-filled"><?php _e('Read More', 'redrock'); ?></a>
			</div>
		</div>
	
	</div>
</div>

<div class="map row-padding txt-cntr">
	<h2><?php _e('Contact us', 'redrock'); ?></h2>
	<div style="padding: 40px 40px 0 40px;">
		<div id="map-canvas"></div>
	</div>
</div>

<div class="contact-bar">

	<div class="row-full">
		<div class="one-third txt-cntr">
			<h3><?php _e('Address', 'redrock'); ?></h3>
			<p>Vliegpleinkouter 35/201<br />
			9030 Mariakerke<br />
			<?php _e('Belgium', 'redrock'); ?></p>
		</div>
		<div class="one-third txt-cntr">
			<h3><?php _e('Contact Details', 'redrock'); ?></h3>
			<p>Tel.: +32 (0) 475 75 30 59<br />
			<a href="mailto:info@janvanghent.com">info@janvanghent.com</a></p>
		</div>
		<div class="one-third soc-med txt-cntr">
			<h3><?php _e('Social Media', 'redrock'); ?></h3>
			<a href="https://www.facebook.com/janvanghent" target="_blank"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/fb-large.png" alt="Facebook" /></a>
			<a href="https://foursquare.com/v/jan-van-ghent/53a31268498e3f7fb3122089" target="_blank"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/foursquare-large.png" alt="Foursquare" /></a>
		</div>
	</div>

</div>

<div class="newsletter-optin">

	<div class="wrap row-padding txt-cntr">
		<h2><?php _e('Our newsletter', 'redrock'); ?></h2>
		<p><?php _e('Subscribe to our newsletter and stay informed about our upcoming activiteits and promotions.', 'redrock'); ?></p>
		
		<div class="row">
			<?php echo do_shortcode('[mc4wp_form]'); ?>
		</div>

	</div>
</div>

<?php get_footer(); ?>