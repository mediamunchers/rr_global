<?php get_header(); ?>
<div class="main">

	<?php if( have_posts() ) :
		while( have_posts() ) :
			the_post();
		?>

			<?php if( has_post_thumbnail() ) :
				?><div class="page-header"><?php
				the_post_thumbnail( );
				?></div><?php
			endif; ?>

			<div class="wrap row-padding txt-cntr">
			
				<?php get_template_part('content', 'page' ); ?>

			</div>

		<?php
		endwhile;
	endif;
	?>

</div>

<?php get_footer(); ?>