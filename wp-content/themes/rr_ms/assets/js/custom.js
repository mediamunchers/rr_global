jQuery(document).ready(function($) {

	// Responsive navigation
	$('.mbl-site-nav').hide();
	$('.nav-mbl').click( function(){

		$('.mbl-site-nav').slideToggle();

	});

	//Fancybox
	$('.gallery-item a').fancybox({
		padding: 10,
		closeBtn : false,
		arrows: false,
		helpers: { 
			title: null,
			buttons: {},
			overlay : { css : { 'background' : 'rgba(255,255,255,0.9)' }, locked: false }
		},
	});

	//Gallery hovers
	$('.gallery-item').hover( function(){
		$(this).find('.main-gallery-overlay').stop(true).fadeIn(200);
	}, function() {
		$(this).find('.main-gallery-overlay').stop(true).fadeOut(200);
	});

	//Datepicker
    $( "#av_checkin" ).datepicker({
      minDate: 1,
      changeMonth: true,
      numberOfMonths: 1,
      altFormat : 'yy-mm-dd',
      altField : '#alt_av_checkin',
      dateFormat : 'dd/mm/yy',
      onSelect: function( selectedDate ) {
        $( "#av_checkout" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#av_checkout" ).datepicker({
      minDate: 1,
      changeMonth: true,
      numberOfMonths: 1,
      altFormat : 'yy-mm-dd',
      altField : '#alt_av_checkout',
      dateFormat : 'dd/mm/yy'
    });

    //Availability check
	$('#av_check').click( function(){
		var av_checkin = $("#alt_av_checkin").val();
		var av_checkout = $("#alt_av_checkout").val();

	    var dateParts_checkin = av_checkin.split('-');
	    var dateParts_checkout = av_checkout.split('-');

		var checkin_date = new Date(dateParts_checkin[0], parseInt(dateParts_checkin[1], 10) - 1, dateParts_checkin[2]);
		var checkout_date = new Date(dateParts_checkout[0], parseInt(dateParts_checkout[1], 10) - 1, dateParts_checkout[2]);

		var days = (checkout_date.getTime() - checkin_date.getTime()) / 86400000;
		var count = 0;
		for ( var i = 0; i < days; i++ ) {
			checkin_date.setDate(checkin_date.getDate() + 1);
			var curr_date = checkin_date.getDate();
			var curr_month = checkin_date.getMonth() + 1;
			var curr_year = checkin_date.getFullYear();
			av_checkin = curr_year+"-"+curr_month+"-"+curr_date;
			$(".reservation-item").each(function(){
				var fully_booked_days = $(this).attr("data-fully-booked-days");
				if(fully_booked_days.indexOf(av_checkin) > 0){
					$(this).addClass('overlay');
					count++;
				}
			});
		}
		if(count === 0){
			$(".reservation-item").each(function(){
				$(this).addClass('overlay');
			});
			window.setTimeout(function(){
				$(".reservation-item").each(function(){ $(this).removeClass('overlay'); });
			}, 100);
		}
	});

});