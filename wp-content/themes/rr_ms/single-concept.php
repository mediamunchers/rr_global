<?php get_header('blog'); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<?php if ( has_post_thumbnail() ) { ?>
	<div class="slider">
		<div class="mbl-slide">
			<?php the_post_thumbnail( 'large' ); ?>
		</div>
		<div class="dsktp-slide">
			<?php the_post_thumbnail( 'large' ); ?>
		</div>
	</div>
	<?php } ?>

	<div class="row-padding txt-cntr">
		<div class="wrap blogpost">
			<h2 class="page-title"><?php the_title(); ?></h2>

			<?php the_content(); ?>

				<?php
				if( have_rows('concept_link') ):
				?>
				<h3><?php _e("Interesting links", "redrock"); ?></h3>
				<ul>
				<?php
				    while ( have_rows('concept_link') ) : the_row();
						$link_url = get_sub_field('concept_link_url');
						$link_title = get_sub_field('concept_link_title');
				        echo '<li><a href="'.$link_url.'" target="_blank">'.$link_title.'</a></li>';       
				    endwhile;
				echo "</ul>";
				endif;
				?>
		</div>
	</div>
<?php endwhile; endif; ?>
<?php get_footer(); ?>