<?php get_header('blog'); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<?php if ( has_post_thumbnail() ) { ?>
	<div class="slider">
		<div class="mbl-slide">
			<?php the_post_thumbnail( 'large' ); ?>
		</div>
		<div class="dsktp-slide">
			<?php the_post_thumbnail( 'large' ); ?>
		</div>
	</div>
	<?php } ?>

	<div class="row-padding">
		<div class="wrap blogpost">
			<h2 class="page-title txt-cntr"><?php the_title(); ?></h2>
			<div class="date txt-cntr"><?php the_author(); ?> - <?php the_date(); ?></div>

			<?php the_content(); ?>
		</div>
		<div class="txt-cntr">
			<?php previous_post_link('%link', '<span class="previous btn-filled">'.__('Previous Post', 'redrock').'</span>', TRUE); ?>
			<?php next_post_link('%link', '<span class="next btn-filled">'.__('Next Post', 'redrock').'</span>', TRUE); ?>
		</div>
	</div>
<?php endwhile; endif; ?>
<?php get_footer(); ?>