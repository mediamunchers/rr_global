<?php
// File Security Check
if ( ! empty( $_SERVER['SCRIPT_FILENAME'] ) && basename( __FILE__ ) == basename( $_SERVER['SCRIPT_FILENAME'] ) ) {
    die ( 'You do not have sufficient permissions to access this page' );
}
?>
<?php

/*-----------------------------------------------------------------------------------*/
/* Register CPT
/*-----------------------------------------------------------------------------------*/

add_action( 'init', 'custom_post_type_events' );

function custom_post_type_events() {
		  
	$labels = array(
		'name' => __('Events', 'customtheme'),
		'singular_name' => __('Events', 'customtheme'),
		'add_new' => __('Add New', 'customtheme'), __('Event', 'customtheme'),
		'add_new_item' => __('Event', 'customtheme'),
		'edit_item' => __('Edit Event', 'customtheme'),
		'new_item' => __('New Event', 'customtheme'),
		'view_item' => __('View Event', 'customtheme'),
		'search_items' => __('Search Events', 'customtheme'),
		'not_found' =>  __('No Events found', 'customtheme'),
		'not_found_in_trash' => __('No Events found in Trash', 'customtheme'),
		'parent_item_colon' => ''
	);

	$supports = array(
		'title',
		'editor',
		'categories',
		'excerpt',
		'thumbnail'
	);

	$capabilities = array(
		'publish_posts' => 'activate_plugins',
        'edit_posts' => 'activate_plugins',
        'edit_others_posts' => 'activate_plugins',
        'delete_posts' => 'activate_plugins',
        'delete_others_posts' => 'activate_plugins',
        'read_private_posts' => 'activate_plugins',
        'edit_post' => 'activate_plugins',
        'delete_post' => 'activate_plugins',
        'read_post' => 'activate_plugins',
	);
  
	register_post_type( 'event',
		array(
			'labels' => $labels,
			'public' => true,
			'menu_position' => 5,
			'hierarchical' => false,
			'has_archive' => true,
			'menu_icon' => 'dashicons-calendar',
			'supports' => $supports,
			'capabilities' => $capabilities
		)
	);
	
}


// Custom Taxonomy
add_action( 'init', 'custom_taxonomy_eventCategories', 0 );  

function custom_taxonomy_eventCategories() {
		
	// Recipe Type Custom Taxonomy
	$recipe_type_labels = array(
	    'name' => __('Event Categories', 'customtheme'),
	    'singular_name' => __('Event Category', 'customtheme'),
	    'search_items' => __('Search Event Categories', 'customtheme'),
	    'all_items' => __('All Event Categories', 'customtheme'),
	    'parent_item' => __('Parent Event Category', 'customtheme'),
	    'parent_item_colon' =>__('Parent Event Category:', 'customtheme'),
	    'edit_item' => __('Edit Event Category', 'customtheme'), 
	    'update_item' => __('Update Event Category', 'customtheme'),
	    'add_new_item' => __('Add Event Category', 'customtheme'),
	    'new_item_name' => __('Event Category Name', 'customtheme'),
	    'menu_name' => __('Event Categories', 'customtheme')
	  ); 


	register_taxonomy(
	    'event_categories',
	    array( 'event', 'tutor' ),
	    array(
			  'hierarchical' => true,
			  'labels' => $recipe_type_labels,
			  'query_var' => true,
	    )
	);
}

?>