<?php global $wpo_wcpdf, $items;
$items = $wpo_wcpdf->get_order_items();
?>
<table class="head container">
	<tr>
		<td class="header">
			<?php
			if( $wpo_wcpdf->get_header_logo_id() ) {
				$wpo_wcpdf->header_logo();
			} else {
				_e( 'Invoice', 'wpo_wcpdf' );
			}
			?>
			<br /><span style="font-weight: 700;">not-for-profit international association</span><br />
			SPACE HEAD OFFICE<br />
			Premises VLHORA/EURASHE<br />
			Ravensteingalerij 27/3<br />
			1000 Brussels
		</td>
		<td class="shop-info">
			<div class="recipient-address"><?php $wpo_wcpdf->billing_address(); ?></div>
		</td>
	</tr>

</table><!-- head container -->

<table style="width: 100%;">
	<tr>
		<td style="font-size: 12pt; line-height: 11pt; font-weight: 700; text-align: right;">
			<?php
			echo 'INVOICE';
			?>
			<span style="display: block; font-weight:700; font-size:8pt;">This invoice is valid as confirmation for your enrolment!</span>
		</td>
	</tr>
	<tr style="">
		<td style="padding: 20px 0;">

			<table style="width: 100%; text-align: center; border-top: 1px solid #ccc; border-bottom: 1px solid #ccc;">
				<tr>
					<th>
						Registration Date
					</th>
					<th>
						Invoice Date
					</th>
					<th>
						Invoice Number
					</th>
					<th>
						Order number
					</th>
					<th>
						VAT
					</th>
				</tr>
				<tr>
					<td style="padding-bottom: 10px;">
						<?php echo $wpo_wcpdf->export->order->order_date; ?>
					</td>
					<td>
						<?php
						$order_date = $wpo_wcpdf->export->order->modified_date;
						echo substr($order_date, 0, strpos($order_date, " "));
						?>
					</td>
					<td>
						<?php echo $wpo_wcpdf->export->invoice_number; ?>
					</td>
					<td>
						<?php
						$ordernr = $wpo_wcpdf->get_conf_ordernr();
						echo $ordernr; ?>
					</td>
					<td>
						<?php
						$vat = $wpo_wcpdf->get_vat_nr();
						echo $vat;
						?>
					</td>
				</tr>
			</table>

		</td>
	</tr>
</table>

<table class="invoice_fee" style="width:100%;">
	<tr>
		<td colspan="2">
			<strong><?php _e('Invoice specification: ', 'wpo_wcpdf'); ?></strong>
		</td>
	</tr>
	<tr>
		<td>
			<?php _e('Registration fee: ' . $wpo_wcpdf->get_conf_fee_name(), 'wpo_wcpdf'); ?>
		</td>
		<td style="text-align: right;">
			<?php $fee_price = $wpo_wcpdf->get_conf_fee_price(); ?>
			<?php echo '&euro; ' . $fee_price ?>
		</td>
	</tr>
</table>

<table class="invoice_options" style="margin-top: 20px; width:100%; border-bottom: 2px solid #ccc;">

	<tr>
		<td>
			<strong><?php _e('Registered for: ', 'wpo_wcpdf'); ?></strong>
		</td>
		<td>
		</td>
		<td>
		</td>
	</tr>


	<?php
	$meals_array = $wpo_wcpdf->get_conf_meals();
	if( $meals_array && $meals_array[0] != "" ){ ?>
		<?php foreach( $meals_array as $meals ){ ?>
		<tr>
			<td>
			</td>
			<td>
		
				<?php foreach( $meals as $meal ){
					echo $meal[0] . '<br />';
				} ?>

			</td>
			<td style="text-align: right;">
				
				<?php foreach( $meals as $meal ){
					echo '&euro; ' . $meal[1] . '<br />';
					$meals_total += $meal[1];
				} ?>

			</td>
		</tr>
		<?php } ?>
	<?php } ?>



	<?php
	$events_array = $wpo_wcpdf->get_conf_events();
	if( $events_array && $events_array[0] != "" ){ ?>
		<?php foreach( $events_array as $events ){ ?>
		<tr>
			<td>
			</td>
			<td>
		
				<?php foreach( $events as $event ){
					echo $event[0] . '<br />';
				} ?>

			</td>
			<td style="text-align: right;">
				
				<?php foreach( $events as $event ){
					echo '&euro; ' . $event[1] . '<br />';
					$events_total += $event[1];
				} ?>

			</td>
		</tr>
		<?php } ?>
	<?php } ?>



	<?php
	$exc_array = $wpo_wcpdf->get_conf_exc();
	if( $exc_array && $exc_array[0] != "" ){ ?>
		<?php foreach( $exc_array as $excs ){ ?>
		<tr>
			<td>
			</td>
			<td>
		
				<?php foreach( $excs as $exc ){
					echo $exc[0] . '<br />';
				} ?>

			</td>
			<td style="text-align: right;">
				
				<?php foreach( $excs as $exc ){
					echo '&euro; ' . $exc[1] . '<br />';
					$exc_total += $exc[1];
				} ?>

			</td>
		</tr>
		<?php } ?>
	<?php } ?>

	<tr style="padding-top: 10px; margin-top: 10px;">
		<td style="padding-top: 10px; padding-bottom: 10px;">
			<strong><?php _e('Total amount in Euro', 'wpo_wcpdf'); ?><strong>
		</td>
		<td style="padding-top: 10px; padding-bottom: 10px;">
			
		</td>
		<td style="padding-top: 10px; padding-bottom: 10px; text-align: right; font-weight: 700;">
			
<?php
$order_total = $fee_price + $meals_total + $events_total + $exc_total;
echo '&euro; ' . $order_total;
?>

		</td>
	</tr>
</table>

<table class="invoice_notice" style="padding: 20px 0;">
	<tr>
		<td style="font-weight: 700;">
			Payment within 10 days<br />
			Please only pay by making an international bank transfer. Cheques are not accepted.
		</td>
	</tr>
</table>

<table style="margin-top: 50px;">
	<tr>
		<td colspan="2">
			<strong>Please state invoice number and name of institute and delegate to:</strong>
		</td>
	</tr>
	<tr>
		<td style="padding: 10px 0;">
			Beneficiary's bank:
		</td>
		<td style="padding: 10px 0;">
			BeObank NV<br />
			Gen. Jacqueslaan 236g, 1015 Brussel<br />
			(Branch: BeObank Gent Kouter, Kouter 146, 9000 GENT)
		</td>
	</tr>
	<tr>
		<td style="padding: 10px 0;">
			Bank account
		</td>
		<td style="padding: 10px 0;">
			n° 953-0280632-77<br />
			of « SPACE Ivzw »
		</td>
	</tr>
	<tr>
		<td style="padding: 10px 0;">
			Swift Code
		</td>
		<td style="padding: 10px 0;">
			CTBK BEBX
		</td>
	</tr>
	<tr>
		<td style="padding: 10px 0;">
			IBAN Code
		</td>
		<td style="padding: 10px 0;">
			BE80 953 028063277
		</td>
	</tr>
</table>

<table class="extra_invoice_text" style="padding-top: 20px;">
	<tr>
		<td>
			<?php
			$raw_extra_invoice_date = $wpo_wcpdf->get_conf_extra_date();
			$extra_invoice_date = date("d F, Y", strtotime($raw_extra_invoice_date));
			?>
			For cancellations before <?php echo $extra_invoice_date; ?>, the fee will be refunded minus a 50 euro handling fee. After <?php echo $extra_invoice_date; ?> SPACE will refund what will be left after covering all cancellations fees. Please contact your hotel to arrange your hotel cancellation
		</td>
	</tr>
</table>

<table style="margin-top: 30px;">
	<tr>
		<td style="font-weight: 700;">Please note that in case no payment has been received one week prior to the Conference we will only accept attendance upon cash payment at the conference.</td>
	</tr>
	<tr>
		<td style="text-align: right; padding-top: 50px;">
			On behalf of SPACE,<br />
			Luc Broes, Financial Manager
		</td>
	</tr>
</table>