<?php

class WC_Paybox_Threetime_Gateway extends WC_Paybox_Abstract_Gateway {
	protected $defaultTitle = 'Paybox 3 times payment';
	protected $defaultDesc = 'xxxx';
	protected $type = 'threetime';

	public function __construct() {
		// Some properties
		$this->id = 'paybox_3x';
		$this->method_title = __('Paybox 3 times', WC_PAYBOX_PLUGIN);
		$this->has_fields = false;
		//$this->icon = TODO;

		parent::__construct();
	}
}
