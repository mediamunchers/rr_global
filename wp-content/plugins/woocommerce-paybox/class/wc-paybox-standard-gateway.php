<?php

class WC_Paybox_Standard_Gateway extends WC_Paybox_Abstract_Gateway {
	protected $defaultTitle = 'Paybox payment';
	protected $defaultDesc = 'xxxx';
	protected $type = 'standard';

	public function __construct() {
		// Some properties
		$this->id = 'paybox_std';
		$this->method_title = __('Paybox', WC_PAYBOX_PLUGIN);
		$this->has_fields = false;
		//$this->icon = TODO;
		//$this->icon              = apply_filters( 'woocommerce_paypal_icon', WC()->plugin_url() . '/assets/images/icons/paypal.png' );

		parent::__construct();
	}
}
