<?php
/**
 * Plugin Name: WooCommerce Paybox Payment plugin
 * Description: Paybox gateway payment plugins for WooCommerce
 * Version: 0.9.0
 * Author: BM Services
 * Author URI: http://www.bm-services.com/
 * 
 * @package WordPress
 * @since 0.9.0
 */
// Ensure not called directly
if (!defined('ABSPATH')) {
	exit;
}

// Ensure WooCommerce is active
if (!in_array('woocommerce/woocommerce.php',
	apply_filters('active_plugins', get_option('active_plugins')))) {
	return;
}

define('WC_PAYBOX_PLUGIN', 'woocommerce-paybox');
define('WC_PAYBOX_VERSION', '0.9.1');

function woocommerce_paybox_installation() {
	global $wpdb;

	include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	if( !is_plugin_active('woocommerce/woocommerce.php') ) {
		_e('WooCommerce mist be activated', WC_PAYBOX_PLUGIN);
		die();
	}

	require_once(ABSPATH.'wp-admin/includes/upgrade.php');

	$tableName = $wpdb->prefix .'wc_paybox_payment';
	$sql = 'create table '.$tableName.'('
		.'id int not null auto_increment primary key,'
		.'order_id bigint not null,'
		.'type enum("capture", "first_payement", "second_payment", "thrid_payment") not null,'
		.'data varchar(2048) not null,'
		.'key order_id (order_id)'
		.')';
	dbDelta($sql);

	update_option(WC_PAYBOX_PLUGIN.'_version', WC_PAYBOX_VERSION);
}

function woocommerce_paybox_initialization() {
	$class = 'WC_Paybox_Abstract_Gateway';

	if (!class_exists($class)) {
		require_once(dirname(__FILE__).'/class/wc-paybox-config.php');
		require_once(dirname(__FILE__).'/class/wc-paybox-iso4217currency.php');
		require_once(dirname(__FILE__).'/class/wc-paybox.php');
		require_once(dirname(__FILE__).'/class/wc-paybox-abstract-gateway.php');
		require_once(dirname(__FILE__).'/class/wc-paybox-standard-gateway.php');
		require_once(dirname(__FILE__).'/class/wc-paybox-threetime-gateway.php');
	}

	load_plugin_textdomain(WC_PAYBOX_PLUGIN, false, dirname(plugin_basename(__FILE__)).'/lang/');
}

function woocommerce_paybox_register(array $methods) {
	$methods[] = 'WC_Paybox_Standard_Gateway';
	$methods[] = 'WC_Paybox_Threetime_Gateway';
	return $methods;
}

register_activation_hook(__FILE__, 'woocommerce_paybox_installation');
add_action('plugins_loaded', 'woocommerce_paybox_initialization');
add_filter('woocommerce_payment_gateways', 'woocommerce_paybox_register');
